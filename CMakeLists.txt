cmake_minimum_required(VERSION 2.6 FATAL_ERROR)
cmake_policy(VERSION 2.6)

project(Split_AudioVideo)

include_directories("${CMAKE_CURRENT_SOURCE_DIR}/.")

set(ALL_SOURCES
  src/elementaryStream.cpp
  src/bitstream.cpp
  src/parse_h264.cpp
  src/parse_AAC.cpp
  src/parse_TS.cpp
  src/split_audiovideo)

add_executable(split_audio_video ${ALL_SOURCES})

